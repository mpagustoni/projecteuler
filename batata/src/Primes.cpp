#include "Primes.h"

Primes::Primes()
{
    this->head = new Prime(2);
    this->last = new Prime(3);
    this->head->next = this->last;
}

Primes::~Primes()
{
    Prime *aux = this->head;
    do{
        aux->~Prime();
    }while(aux->next != nullptr);
}

unsigned long long int Primes::findNextPrime()
{
    unsigned long long int numberToTest = this->last->number;
    Prime *actualPrime = this->head;
    bool isDivisible;
    do
    {

        while(isDivisible || actualPrime->number < this->last->number)
        {
            isDivisible = (numberToTest % actualPrime->number == 0);
            actualPrime = actualPrime->next;
        }
    }while(isDivisible || );

    Prime *newPrime = new Prime(number);
    this->last->next = newPrime;
    this->last = newPrime;
    return number;
}
