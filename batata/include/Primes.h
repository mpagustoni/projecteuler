#ifndef PRIMES_H
#define PRIMES_H
#include "Prime.h"

class Primes
{
    public:
        Primes();
        virtual ~Primes();
        Prime *head;
        Prime *last;
        unsigned long long int findNextPrime();
    protected:

    private:
};

#endif // PRIMES_H
