#ifndef PRIME_H
#define PRIME_H


class Prime
{
    public:
        Prime(unsigned long long int number);
        virtual ~Prime();
        unsigned long long int number;
        Prime *next;
    protected:

    private:
};

#endif // PRIME_H
