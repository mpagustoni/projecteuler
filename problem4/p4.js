/*
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

 Find the largest palindrome made from the product of two 3-digit numbers.
 */

var palindromes = [];
for (i = 999; i >= 100; i--) {
    for (j = i; j >= 100; j--) {
        saveIfPalindrome(i * j);
    }
}
palindromes.sort(function (a, b) { return b - a });
console.log(palindromes[0]);

function saveIfPalindrome(raw) {
    num = raw.toString();
    var rev = num.split('').reverse().join('');
    if (num === rev) {
        palindromes.push(raw);
    }
}