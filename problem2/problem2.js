const limit = 4000000;

var fib1 = 1, fib2 = 2;
var sum = 0;

do{
    if( !(fib2 % 2)){
        sum += fib2;
    }
    var next = fib1 + fib2;
    fib1 = fib2;
    fib2 = next;
}while(fib2 < limit);

console.log(sum);