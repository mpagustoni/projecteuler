var input = 600851475143;
var primes = [2];
var i = 0;
var prime = 2;
do {
    do {
        var remain = 0;
        remain = input % prime;
        if (remain == 0) {
            input = input / prime;
            console.log("divided by " + prime);
        }
    } while (remain == 0);
    prime = findNextPrime();
} while (input != 0);





function getLastPrime() {
    return primes[primes.length - 1];
}

function findNextPrime() {
    var last = getLastPrime();
    var remain = 0;
    do {
        last++;
        var i = 0;
        do {

            var remain = last % primes[i];
            i++;
        } while (remain != 0 && primes.length >= i);
    } while (primes.length >= i);
    primes.push(last);
    //console.log(last);

    return last;
}

